<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pages | CustomCMS</title>
    <link rel="stylesheet" href="https://unpkg.com/grapesjs/dist/css/grapes.min.css">
    <style>
        .gjs-devices-c {
            padding: 0;
        }
    </style>
</head>
<body>
    <div id="gjs"></div>

    <script src="https://unpkg.com/grapesjs"></script>
    <script src="https://cdn.jsdelivr.net/npm/grapesjs-blocks-basic@0.1.8/dist/grapesjs-blocks-basic.min.js"></script>
    <script src="https://unpkg.com/grapesjs-blocks-flexbox"></script>
    <script src="https://unpkg.com/grapesjs-tooltip"></script>
    <script type="text/javascript">
        var editor = grapesjs.init({
            container : '#gjs',
            components: '<div class="txt-red">Hello world!</div>',
            style: '.txt-red{color: red}',
            plugins: [
                'gjs-blocks-basic',
                'gjs-blocks-flexbox',
                'grapesjs-tooltip',
            ],
        });
    </script>
</body>
</html>

{{-- @extends('adminlte::page') --}}

{{-- @section('title', 'Pages | CustomCMS') --}}
{{-- @section('css') --}}
    
{{-- @stop --}}

{{-- @section('content_header')
    <h1>Pages</h1>
@stop --}}

{{-- @section('content') --}}
    
{{-- @stop --}}

{{-- @section('js') --}}
    
{{-- @stop --}}