@extends('adminlte::page')

@section('title', 'Roles | CustomCMS')

@section('content_header')
    <h1>Roles</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                            {{ Session::get('message') }}
                        </p>
                    @endif
                </div>
                <div class="box-body">
                    <table id="laravel_datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Role</th>
                                <th>Slug</th>
                                <th>Description</th>
                                <th>Level</th>
                                <th>Permissions</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->slug }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>{{ $role->level }}</td>
                                    <td>
                                        @foreach($role->permissions as $permission)
                                            @if($role->permissions->last() === $permission)
                                                <b>{{ $permission->name }}</b>
                                            @else
                                                <b>{{ $permission->name }}</b>,
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ $role->created_at }}</td>
                                    <td>{{ $role->updated_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@stop

@section('js')
<script>
    $(document).ready(function() {
        $('#laravel_datatable').DataTable();
    });
</script>
@stop