<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware(['verified'])->group(function() {
    Route::get('profile', 'UserController@profile')->name('profile')->middleware('verified');
// });
Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('dashboard')->middleware('verified');
Route::get('/admin/user', 'Admin\UserController@index')->middleware('verified');
Route::get('/admin/pages', 'Admin\DashboardController@pages')->middleware('verified');
Route::get('/admin/roles', 'Admin\RolesController@roles')->middleware('verified');