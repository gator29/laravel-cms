<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use jeremykenedy\LaravelRoles\Traits\RolesAndPermissionsHelpersTrait;
use jeremykenedy\LaravelRoles\Traits\RoleHasRelations;
use Datatables;
use App\User;
use DB;

class RolesController extends Controller
{

    use RolesAndPermissionsHelpersTrait, RoleHasRelations;

    public function roles() {
        $roles = DB::table(config('roles.rolesTable'))->get();
        
        foreach($roles as $role) {
            $permissions = $this->getRolePermissions($role->id);
            $role->permissions = $permissions;
        }

        return view('admin.roles.list', ['roles' => $roles]);
    }
}
